import Savoir
import os
from fileparser import ConfigFileParser
from MultichainPython import Multichainpython
from pubnub import PubNub
#TestMultichain

class TestMultichain:
    def __init__(self,rpcuser,rpcpasswd,rpchost,rpcport,chainname):
                self.rpcuser = rpcuser
                self.rpcpasswd = rpcpasswd
                self.rpchost = rpchost
                self.rpcport = rpcport
                self.chainname = chainname
                self.mchain = Multichainpython(self.rpcuser,self.rpcpasswd,self.rpchost,self.rpcport,self.chainname)

    def connectTochain(self):
        print("connected")
        print(chainname)
        return self.mchain.multichainConnect()

    def assetsubscribe(self,asset):
                print(asset)
                self.mchain.subscribeToasset(asset)
    def assetbalances(self):
                assetbalances = self.mchain.gettotalbalances()
                return assetbalances
    def queryassetdetails(self,asset):
                assetdetails = self.mchain.queryassetsdetails(asset)
                #print(assetdetails)
                return assetdetails
    def getasset(self, asset):
        try:
            self.assetsubscribe(assetname)
            assetdetailss = self.mchain.queryasset(asset)
            getasset_return = list(assetdetailss)[7]
            message = {"op_return":getasset_return}
    		publish_handler({"node":"testmultichain","messagecode":"getassetinfo","messagetype":"resp","message":message})
            print(list(assetdetailss)[7])
        except Exception as e:
		    print e,"error in getasset"
		    message = {"op_return":"error","message":e}
		    publish_handler({"node":"testmultichain","messagecode":"getassetinfo","messagetype":"resp","message":message})

def pub_Init(): 
	global pubnub
	try:
        pubnub = PubNub(publish_key=pub_key,subscribe_key=sub_key) 
	    pubnub.subscribe(channels=pubchannel, callback=callback, error=error,
	    connect=connect, reconnect=reconnect, disconnect=disconnect)    
	    return True
	except Exception as pubException:
	    print("The pubException is %s %s"%(pubException,type(pubException)))
	    return False    

def callback(message,channel):
	try:
		print message
		if message["messagetype"] == "req":
			if message["messagecode"] == "getassetinfo":
				TM.getasset("asset1")
	except Exception as e:
		print("The callback exception is %s,%s"%(e,type(e)))


def publish_handler(message):
	try:
	    pbreturn = pubnub.publish(channel = pubchannel ,message = message,error=error)

	except Exception as e:
	    print ("The publish_handler exception is %s,%s"%(e,type(e)))

def error(message):
    print("ERROR on Pubnub: " + str(message))

def connect(message):
    print("CONNECTED")

def reconnect(message):
	print("RECONNECTED")

def disconnect(message):
	print("DISCONNECTED")
                


if __name__ == '__main__':
        pubnub = None
        filename = "config.ini"
        cf = ConfigFileParser()
        cf.parseConfig(filename)
         # Multichain  Credentials
        rpcuser= cf.getConfig("rpcuser")
        rpcpasswd=cf.getConfig("rpcpasswd")
        rpchost = cf.getConfig("rpchost")
        rpcport = cf.getConfig("rpcport")
        chainname = cf.getConfig("chainname")
        pubchannel = cf.getConfig("pubchannel")
        pub_key = cf.getConfig("pubkey")
	    sub_key = cf.getConfig("subkey")  

        # Initializing the TestMultichain class
        TM = TestMultichain(rpcuser,rpcpasswd,rpchost,rpcport,chainname)
        TM.connectTochain()
        TM.assetsubscribe("asset1")
        TM.queryassetdetails("asset1")
        TM.getasset("asset1")

        pub_Init()

